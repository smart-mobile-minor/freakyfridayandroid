package com.example.freakyfriday

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.freakyfriday.AddComponentRecycler.PartSelectionAdapter
import com.example.freakyfriday.adapters.PartSelectAdapter
import com.example.freakyfriday.objects.Processor
import com.example.freakyfriday.objects.SelectItem
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_beeps.*

/**
 * A simple [Fragment] subclass.
 * Use the [Beeps.newInstance] factory method to
 * create an instance of this fragment.
 */
class Beeps : Fragment() {
    val lvItems: MutableList<Any> = mutableListOf<Any>()
    val partSelectionAdapter: PartSelectionAdapter = PartSelectionAdapter(lvItems)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_beeps, container, false)
        val ctx: Context = requireContext()
        LoadRecyclerViewData(view, ctx)
        SetOnClickFab(view)
        return view
    }

    fun SetOnClickFab(view:View){
        val btn = view.findViewById<FloatingActionButton>(R.id.fabRecord)
        btn.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

    }

    fun LoadRecyclerViewData(view: View, ctx: Context){
        val select = SelectItem("Add processor")
        val processor = Processor("Ryzen ThreadRipper", "AMD", "sTRX4")
        lvItems.add(processor)
        partSelectionAdapter.items = lvItems
        partSelectionAdapter.notifyDataSetChanged()
        val lv = view.findViewById<RecyclerView>(R.id.lvSelectPart)

        lv.layoutManager = LinearLayoutManager(ctx)
        lv.adapter = partSelectionAdapter
        lv.addItemDecoration(DividerItemDecoration(ctx, DividerItemDecoration.VERTICAL))


    }


}