package com.example.freakyfriday

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.TextView
import com.example.freakyfriday.adapters.PartSelectAdapter
import com.example.freakyfriday.objects.Processor
import com.example.freakyfriday.objects.SelectItem
import kotlinx.android.synthetic.*
import kotlinx.coroutines.NonDisposableHandle.parent


/**
 * A simple [Fragment] subclass.
 * Use the [SelectPart.newInstance] factory method to
 * create an instance of this fragment.
 */
class SelectPart : Fragment() {
    val lvItems: MutableList<Any> = mutableListOf<Any>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(com.example.freakyfriday.R.layout.fragment_select_part, container, false)
        val ctx: Context = requireContext()

        Loadlv(view, ctx)
        return view
    }

    fun Loadlv(view: View, ctx: Context){

        val select = SelectItem("Add processor")
        val processor = Processor("Ryzen ThreadRipper", "AMD", "sTRX4")
        lvItems.add(select)
        lvItems.add(processor)
        var lv = view.findViewById<ListView>(R.id.lvSelectPart)
        //lv.isClickable = true
        lv.adapter = PartSelectAdapter(ctx, R.layout.part_listview_template,R.id.lvPartHeader,lvItems)


    }
}