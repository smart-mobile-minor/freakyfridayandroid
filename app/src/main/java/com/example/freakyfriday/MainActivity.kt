package com.example.freakyfriday

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

import android.content.Context
import android.text.Layout

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.ActionBar










class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    var toggle : ActionBarDrawerToggle? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        showActionBar()

        toggle = ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.open, R.string.close)

        drawerLayout.addDrawerListener(toggle!!)

        nav_menu.setNavigationItemSelectedListener(this)
        setToolbarTitle("Home")

        changeFragment(Home())
    }
    private fun showActionBar() {
        val inflator = this
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val v: View = inflator.inflate(R.layout.home_menu, null)
        val actionBar: ActionBar? = supportActionBar
        actionBar!!.setDisplayShowCustomEnabled(true)
        //actionBar.setDisplayShowTitleEnabled(false)
        actionBar.setCustomView(v)
        setOnclickListeners(actionBar)
        var btn = toolbar.findViewById<View>(R.id.tbFilter)
        btn.visibility = View.INVISIBLE
    }

    private fun setOnclickListeners(actionBar: ActionBar){
        actionBar.customView.findViewById<View>(R.id.tbMenu).setOnClickListener { v: View? ->
            togglesync(toggle!!)
        }
        actionBar.customView.findViewById<View>(R.id.tbAdd).setOnClickListener { v: View? ->
            navigate(getString(R.string.build), PartSelection())
        }
//        actionBar.customView.findViewById<View>(R.id.tbFilter).setOnClickListener { v: View? ->
//            togglesync(toggle!!)
//        }
    }

    fun togglesync(toggle: ActionBarDrawerToggle){
        drawerLayout.openDrawer(GravityCompat.START)
    }




    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        drawerLayout.closeDrawer(GravityCompat.START)

        when(item.itemId) {
            R.id.nav_Home -> navigate(getString(R.string.home), Home())
            R.id.nav_Beeps -> navigate(getString(R.string.beeps), Beeps())
            R.id.nav_Settings -> navigate(getString(R.string.settings), Settings())
        }

        return true
    }

    private fun navigate(title:String, frag: Fragment){
        val btn = toolbar.findViewById<View>(R.id.tbAdd)
        btn.visibility = View.INVISIBLE
        setToolbarTitle(title)
        changeToolbarIcons(title)
        changeFragment(frag)
    }

    private fun changeToolbarIcons(name: String){
        val btnAdd = toolbar.findViewById<View>(R.id.tbAdd)
        val btnFilter = toolbar.findViewById<View>(R.id.tbFilter)
        if(name == getString(R.string.home)){
            btnAdd.visibility = View.VISIBLE
            btnFilter.visibility = View.INVISIBLE
        }
        if(name == getString(R.string.beeps)){
            btnAdd.visibility = View.INVISIBLE
            btnFilter.visibility = View.INVISIBLE
        }
    }

    private fun setToolbarTitle(title: String){
        val tbTitle = toolbar.findViewById<TextView>(R.id.tbTitle)
        tbTitle.text = title
    }

    private fun changeFragment(frag: Fragment){
        val fragment = supportFragmentManager.beginTransaction()
        fragment.replace(R.id.fragment_container, frag).commit()
    }
}