package com.example.freakyfriday.AddComponentRecycler

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.freakyfriday.R
import com.example.freakyfriday.objects.Processor
import kotlinx.android.synthetic.main.selected_processor_listview_template.view.*

class SelectedProcessorViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    fun bind(item: Any){
        if(item is Processor){
            itemView.lvImage.setImageResource(R.mipmap.ic_launcher)
            itemView.btnIcon.setImageResource(R.drawable.ic_icons_delete)
            itemView.lvPartHeader.text = item.name
            itemView.lvPartBrand.text = item.brand
            itemView.lvPartSocket.text = item.socketType
        }
    }
}