package com.example.freakyfriday.AddComponentRecycler

import android.content.Context
import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.example.freakyfriday.Beeps
import com.example.freakyfriday.Home
import com.example.freakyfriday.R
import com.example.freakyfriday.Settings
import com.example.freakyfriday.objects.Processor
import com.example.freakyfriday.objects.SelectItem


class PartSelectionAdapter(var items : MutableList<Any>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    val SELECT = 0
    val SELECTED_PROCESSOR = 1


    private fun inflateLayout(layout: Int, parent: ViewGroup) : View {
        val layoutInflater = LayoutInflater.from(parent.context)
        return layoutInflater.inflate(layout, parent, false)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        when(viewType) {
            SELECT -> {
                return SelectComponentViewHolder(inflateLayout(R.layout.part_listview_template, parent))
            }
            SELECTED_PROCESSOR -> {
                return SelectedProcessorViewHolder(inflateLayout(R.layout.selected_processor_listview_template, parent))
            }
        }
        return SelectComponentViewHolder(inflateLayout(R.layout.part_listview_template, parent))
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(getItemViewType(position)) {
            SELECT -> {
                (holder as SelectComponentViewHolder).bind(items[position])
            }
            SELECTED_PROCESSOR -> {
                (holder as SelectedProcessorViewHolder).bind(items[position])
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        val item: Any = items[position]
        when(item) {
            is SelectItem -> {
                return SELECT
            }
            is Processor -> {
                return SELECTED_PROCESSOR
            }
        }
        return SELECT
    }


}