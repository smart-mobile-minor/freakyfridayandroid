package com.example.freakyfriday.AddComponentRecycler

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.freakyfriday.R
import com.example.freakyfriday.objects.SelectItem
import kotlinx.android.synthetic.main.part_listview_template.view.*

class SelectComponentViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)  {
    fun bind(item: Any){
        if(item is SelectItem){
            itemView.btnIcon.setImageResource(R.drawable.ic_icons_plus)
            itemView.lvPartHeader.text = item.name
        }
    }
}