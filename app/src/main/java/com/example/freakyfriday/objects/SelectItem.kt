package com.example.freakyfriday.objects

data class SelectItem(var name: String, val template: Int = 0)
