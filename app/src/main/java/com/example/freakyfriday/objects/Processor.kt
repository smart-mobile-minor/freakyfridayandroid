package com.example.freakyfriday.objects

data class Processor(val name: String, val brand: String, val socketType: String, val template: Int = 1)
