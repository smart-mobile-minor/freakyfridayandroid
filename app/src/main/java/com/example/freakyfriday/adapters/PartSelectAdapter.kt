package com.example.freakyfriday.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.freakyfriday.R
import com.example.freakyfriday.objects.Processor
import com.example.freakyfriday.objects.SelectItem

class PartSelectAdapter(val ctx: Context, val layout: Int, val resource: Int, val items : MutableList<Any>) :  ArrayAdapter<Any>(ctx,layout , resource ,items){
    val SELECT = 0
    val SELECTED = 1

    override fun getItemViewType(position: Int): Int {
        val item: Any = items[position]
        if(item is SelectItem){
            return SELECT
        }
        else if(item is Processor){
            return SELECTED
        }
        else{
            return SELECT
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val inflater : LayoutInflater = LayoutInflater.from(ctx)
        val lvData: Any = items[position]
        val lvItem: Any = getItemViewType(position)
        var view: View = inflater.inflate(R.layout.part_listview_template, null)


        if(lvItem == SELECT && lvData is SelectItem) {
            view = inflater.inflate(R.layout.part_listview_template, null)

            val icon: ImageView = view.findViewById(R.id.btnIcon)
            val header: TextView = view.findViewById(R.id.lvPartHeader)

            icon.setImageResource(R.drawable.ic_icons_plus)
            header.text = lvData.name

        }
        else if (lvItem == SELECTED && lvData is Processor) {
            view = inflater.inflate(R.layout.selected_processor_listview_template, null)
            val image: ImageView = view.findViewById(R.id.lvImage)
            val icon: ImageView = view.findViewById(R.id.btnIcon)
            val header: TextView = view.findViewById(R.id.lvPartHeader)
            val brand: TextView = view.findViewById(R.id.lvPartBrand)
            val socketType: TextView = view.findViewById(R.id.lvPartSocket)

            image.setImageResource(R.mipmap.ic_launcher)
            icon.setImageResource(R.drawable.ic_icons_delete)
            header.text = lvData.name
            brand.text = lvData.brand
            socketType.text = lvData.socketType
        }



        return view
    }

}