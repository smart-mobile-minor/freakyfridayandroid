package com.example.freakyfriday

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import com.example.freakyfriday.adapters.PartSelectAdapter
import com.example.freakyfriday.objects.Processor
import com.example.freakyfriday.objects.SelectItem


/**
 * A simple [Fragment] subclass.
 * Use the [PartSelection.newInstance] factory method to
 * create an instance of this fragment.
 */
class PartSelection : Fragment() {
    val lvItems: MutableList<Any> = mutableListOf<Any>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(com.example.freakyfriday.R.layout.fragment_part_selection, container, false)
        val ctx: Context = requireContext()

        Loadlv(view, ctx)
        // Inflate the layout for this fragment
        return view
    }

    fun Loadlv(view: View, ctx: Context){


        val processor = Processor("Ryzen ThreadRipper", "AMD", "sTRX4")
        val processor1 = Processor("Ryzen ThreadRipper", "AMD", "sTRX4")
        val processor2 = Processor("Ryzen ThreadRipper", "AMD", "sTRX4")
        val processor3 = Processor("Ryzen ThreadRipper", "AMD", "sTRX4")
        lvItems.add(processor)
        lvItems.add(processor1)
        lvItems.add(processor2)
        lvItems.add(processor3)
        lvItems.add(processor)
        lvItems.add(processor1)
        lvItems.add(processor2)
        lvItems.add(processor3)
        var lv = view.findViewById<ListView>(R.id.lvParts)
        //lv.isClickable = true
        lv.adapter = PartSelectAdapter(ctx, R.layout.part_listview_template,R.id.lvPartHeader,lvItems)


    }
}